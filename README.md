# Installation

## Virtual machine setup

This virtual machine uses a pre-built box that must first be added to your
machine.

Download uwaterloo.package.box and place it in the boxes folder of this project.
Next, issue the following command to add the box.

```
vagrant box add uwaterloo-box file://<project-path>/boxes/uwaterloo.package.box
```

### Using the Virtual machine

#### Running the VM

After installation, run the following command from the project directory.

```
vagrant up && open http://wcms.dev
```

#### SSH Access

After installation, run the following command from the project directory.

```
vagrant ssh
```

#### Shutting down the VM

After installation, run the following command from the project directory.

```
vagrant halt
```

### About Vagrant

Learn more about Vagrant at [https://docs.vagrantup.com/v2/](https://docs.vagrantup.com/v2/).

## Host machine setup

This virtual machine is configured to bind to an IP address: `192.168.50.50`.
We need to direct requests to our host machine to this address.

Add this line to `/etc/hosts`

```
192.168.50.50 wcms.dev
```

## Drupal Setup

Create a new user. Issue the following command from the project directory.

```
drush user-create <username> --mail="<email>" --password="<password>"
```

This command will return the user's ID. We will use that ID to make the user
an administrator. Run the following command:

```
drush user-add-role "administrator" <uid>
```
